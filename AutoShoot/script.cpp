#include "stdafx.h"
#include "inc/main.h"

#include "script.h"

Ped GetPlayerPed()
{
	if (PLAYER::DOES_MAIN_PLAYER_EXIST())
	{
		return PLAYER::GET_PLAYER_PED(PLAYER::GET_PLAYER_ID());
	}

	return NULL;
}

void main()
{
	bool useAmmo = GetPrivateProfileInt(L"SETTINGS", L"USE_AMMO", 1, L".\\AutoShoot.ini") != 0;
	int shootKey = GetPrivateProfileInt(L"SETTINGS", L"SHOOT_KEY", 0x56, L".\\AutoShoot.ini"); //V

	while (true)
	{
		if (!HUD::IS_PAUSE_MENU_ACTIVE() && CUTSCENE::GET_CUTSCENE_TIME_MS() == 0)
		{
			if (IsKeyJustUp(shootKey))
			{
				Ped plrPed = GetPlayerPed();

				if (plrPed != NULL && PED::DOES_PED_EXIST(plrPed) && !PED::IS_PED_DEAD(plrPed))
				{
					Vector3 plrPedPos = PED::GET_PED_COORDS(plrPed);
					Weapon weapon = WEAPON::GET_WEAPON_FROM_HAND(plrPed, 0, true);

					if (!useAmmo || WEAPON::GET_WEAPON_AMMO_IN_CLIP(weapon) > 0)
					{
						Hash weaponHash;
						WEAPON::GET_CURRENT_PED_WEAPON(plrPed, &weaponHash, 0);

						Ped worldPeds[64];
						int numPeds = worldGetAllPeds(worldPeds, 64);

						for (int i = 0; i < numPeds; i++)
						{
							int ammoInClip = WEAPON::GET_WEAPON_AMMO_IN_CLIP(weapon);

							if (useAmmo && ammoInClip == 0) break;

							if (worldPeds[i] != NULL && PED::DOES_PED_EXIST(worldPeds[i]) && !PED::IS_PED_DEAD(worldPeds[i]) && PED::IS_PED_LOS_CLEAR_TO_TARGET_PED(plrPed, worldPeds[i]))
							{
								Vector3 pedPos = PED::GET_PED_COORDS(worldPeds[i]);
								Vector3 pedHeadPos = PED::GET_PED_BONE_COORDS(worldPeds[i], (int)ePedBone::HEAD, 0.0f, 0.0f, 0.0f);

								float heading = MISC::GET_HEADING_FROM_VECTOR_2D(pedPos.x - plrPedPos.x, pedPos.y - plrPedPos.y);

								PED::SET_PED_HEADING(plrPed, heading);

								Vector3 gunPos = PED::GET_PED_BONE_COORDS(plrPed, (int)ePedBone::R_HAND, 0.0f, 0.0f, 0.25f);

								MISC::SHOOT_SINGLE_BULLET_BETWEEN_COORDS(gunPos.x, gunPos.y, gunPos.z, pedHeadPos.x, pedHeadPos.y, pedHeadPos.z, 100, true, weaponHash, plrPed);

								if (useAmmo) WEAPON::SET_WEAPON_AMMO_IN_CLIP(weapon, ammoInClip - 1);

								for (int i = 0; i < 8; i++)
								{
									PED::SET_PED_HEADING(plrPed, heading);

									scriptWait(1);
								}
							}
							else scriptWait(0);
						}
					}
				}
			}
		}

		scriptWait(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
